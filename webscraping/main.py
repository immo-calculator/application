# -*- coding: utf-8 -*-
# Local Imports
from webscraping.tools import settings
from webscraping.database import create_db_connection, execute_query

# Standard Library Imports
import re
import pickle
import logging
from time import sleep

# Additional Library Imports
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as ec

# Advanced settings for browser
# Todo: Load settings in settings file
driver = settings.get_driver(headless=False)
logger = logging.getLogger(__name__)
timeout = 5


# Cookie handling
def check_cookie_box_visibility(trigger_accept_cookies):
    """
    Check function with load wait to check if cookie box is visible and if so then execute script
    Expected inputs:
    bool(trigger_accept_cookies): States if function should call other function to accept cookies
    Todo: Find a workaround for hard coded breaks in DOM objecten in connection with Javascript
    """
    cookie_box_is_visible = bool(False)

    try:
        sleep(2)  # Hardcoded sleep function necessary because of shadow-root DOM element on website
        cookie_box = WebDriverWait(driver, timeout).until(lambda d: d.execute_script(
            'return document.getElementById("usercentrics-root").shadowRoot.'
            'querySelector("div[data-testid]").querySelector("div[data-testid]")'))
        cookie_box_is_visible = True

    except NoSuchElementException:
        print("Element has not been found")

    except TimeoutException:
        print("Timed out waiting for page to load cookie box")

    if cookie_box_is_visible and trigger_accept_cookies:
        accept_cookies()
        return True

    else:
        return False


def accept_cookies():
    """
    Accepts cookies if they have been found by function check_cookie_box_visibility (function returned True)
    """
    javascript = 'return document.getElementById("usercentrics-root").shadowRoot.' \
                     'querySelector("div[data-testid]").querySelector("div[data-testid]").' \
                     'querySelector("footer").querySelector("div[data-testid]").querySelector("div").' \
                     'querySelector("[data-testid=uc-accept-all-button]")'

    cookie_button = driver.execute_script(javascript)
    cookie_button.click()


def get_load_cookies():
    """
    Function is getting and loading/using cookies from site in order to prevent cookie box appearing every time
    """
    pickle.dump(driver.get_cookies(), open("cookies.pkl", "wb"))
    for cookie in pickle.load(open("cookies.pkl", "rb")):
        driver.add_cookie(cookie)


# Webscraping
def webscraping_start_page():
    """
    Function is searching for offers of a specific PLZ on the landing page of https://www.immonet.de/
    Todo: Get PLZ from database
    """
    driver.get("https://www.immonet.de/")
    search_bar = driver.find_element_by_xpath('//*[@id="location"]').send_keys("91154" + Keys.ENTER), sleep(1)
    buy_button = driver.find_element_by_xpath('//*[@id="btn-insearch-marketingtype-buy"]').click(), sleep(1)
    dropdown_box = driver.find_element_by_xpath('//*[@id="estate-type"]').click(), sleep(1)
    house_select = driver.find_element_by_xpath('//*[@id="estate-type"]/option[2]').click(), sleep(1)
    find_button = driver.find_element_by_xpath('//*[@id="btn-int-find-immoobjects"]').click(), sleep(1)


def webscraping_results_page():
    """
    Function is getting all search results of a single page, triggered by webscraping_start_page()
    Todo: Consider other pages in search results
    """
    results = driver.find_elements_by_xpath("//*[@class='col-xs-12 place-over-understitial sel-bg-gray-lighter']"
                                            "/div/div/div//*[@class='flex-grow-1 display-flex-sm']"
                                            "//*[@class='flex-grow-1 display-flex flex-direction-column "
                                            "box-25 overflow-hidden cursor-hand']/div/div/a")

    results_list = []

    for result in results:
        result_item = parse_result(result)
        if result_item[0] == "" or result_item[1] == "" or result_item[2] == "":
            logger.exception("Element will not be inserted to DB as required values are missing.")
        else:
            results_list.append(result_item)

    connection = create_db_connection()
    sql_insert_statement = "INSERT INTO houseprices (zip, price, first_area, ref) VALUES (%s, %s, %s, %s)"
    execute_query(connection, sql_insert_statement, results_list)


def parse_result(result):
    """
    Beschreibung der Funktion
    Parameters: base_url (str): Explain
    Returns: dict: Explain
    Todo: Pass ID from Href; Query if listing already in database + price check
    """
    item_dict = dict(zip='', price='', first_area='', ref='')
    url = result.get_attribute("href")
    javascript = "window.open('" + url + "'"",'_blank')"
    driver.execute_script(javascript)
    driver.switch_to.window(driver.window_handles[1])

    try:
        address_xpath = '//*[@id="top"]/div[1]/div[2]/div[4]/main/div[3]/div[2]/div/div[3]/span/p'
        address_is_present = ec.presence_of_element_located((By.XPATH, address_xpath))
        WebDriverWait(driver, timeout).until(address_is_present)
        address_string = str(driver.find_element_by_xpath(address_xpath).text.replace("\n", " "))

        plz = re.match('^.*(?P<zipcode>\d{5}).*$', address_string).groupdict()['zipcode']
        price = str(driver.find_element_by_xpath('//*[@id="kfpriceValue"]').text.replace(' €', ''))
        first_area = str(driver.find_element_by_xpath('//*[@id="kffirstareaValue"]').text.replace(' m²', ''))
        ref = str(re.sub(r'https://www.immonet.de/angebot/', '', url))

        item_dict.update({"zip": plz, "price": price, "first_area": first_area, "ref": ref})

    except NoSuchElementException:
        logger.exception("Element from url", url, "has not been found as object has no declared price.")

        pass

    sleep(1)
    driver.close()
    driver.switch_to.window(driver.window_handles[0])

    result_item_tuple = tuple(item_dict.values())
    return result_item_tuple


def webscraping_execute():
    """
    Beschreibung der Funktion
    TODO: DEMO-Function
    """
    driver.get("https://www.immonet.de/")
    sleep(3)
    check_cookie_box_visibility('True')
    sleep(2)
    webscraping_start_page()
    sleep(2)
    webscraping_results_page()


webscraping_execute()
