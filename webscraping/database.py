import mysql.connector


def create_db_connection():

    try:
        connection = mysql.connector.connect(option_files="my.ini")
        print("MySQL Database connection successful")

    except ConnectionError as err:
        print(f"Error: '{err}'")

    return connection


def read_query(connection, query):
    cursor = connection.cursor()

    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except ConnectionError as err:
        print(f"Error: '{err}'")


def execute_query(connection, sql_statement, values_list):
    cursor = connection.cursor()

    try:
        cursor.executemany(sql_statement, values_list)
        connection.commit()
        print("Query successful")

    except ConnectionError as err:
        print(f"Error: '{err}'")